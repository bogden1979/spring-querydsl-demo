package jcg.zheng.demo.querydsldemo.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import jcg.zheng.demo.querydsldemo.TestData;
import jcg.zheng.demo.querydsldemo.entity.Company;
import jcg.zheng.demo.querydsldemo.entity.CompanyType;
import jcg.zheng.demo.querydsldemo.entity.Contact;
import jcg.zheng.demo.querydsldemo.entity.ContactType;
import jcg.zheng.demo.querydsldemo.service.CompanyService;
import jcg.zheng.demo.querydsldemo.service.ContactService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ContactQuerydslRepositoryTest extends TestData {

	@Autowired
	private ContactQuerydslRepository demo;

	@Autowired
	private ContactService contactService;

	@Autowired
	private CompanyService companyService;

	@Test
	public void it_should_find_contact_after_saved() {
		Company company = companyService.save(buildTestCompany("Zheng JCG", CompanyType.CUSTOMER));
		Contact mary = contactService.save(company, "Mary", "Zheng", ContactType.PRIMARY);
	
		long contactId = 1L;
		Contact found = demo.findOne(contactId);
		assertNotNull(found);
		assertEquals("Mary", found.getFirstName());
		assertEquals("Zheng", found.getLastName());
		assertEquals(ContactType.PRIMARY, found.getType());
		
		Contact alex = contactService.save(company, "Alex", "Zheng", ContactType.SEONDARY);
		List<Contact> rets = demo.findByCompanyId(company.getId());

		assertNotNull(rets);
		assertEquals(2, rets.size());
		
		contactService.delete(mary);
		contactService.delete(alex);	
	}

}
