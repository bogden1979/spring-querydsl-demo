package jcg.zheng.demo.querydsldemo;

import jcg.zheng.demo.querydsldemo.entity.Company;
import jcg.zheng.demo.querydsldemo.entity.CompanyType;
import jcg.zheng.demo.querydsldemo.entity.Contact;
import jcg.zheng.demo.querydsldemo.entity.ContactType;

public class TestData {

	public TestData() {
		super();
	}

	protected Company buildTestCompany(String name, CompanyType type) {
		Company company = new Company();
		company.setName(name);
		company.setType(type);
		return company;
	}

	protected Contact buildTestContact(String firstName, String lastName, ContactType type) {
		Contact contact = new Contact();
		contact.setFirstName(firstName);
		contact.setLastName(lastName);
		contact.setType(type);
		return contact;
	}

}