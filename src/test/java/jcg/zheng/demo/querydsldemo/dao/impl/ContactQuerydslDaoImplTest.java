package jcg.zheng.demo.querydsldemo.dao.impl;

import static org.junit.Assert.assertTrue;

import java.util.List;

import jcg.zheng.demo.querydsldemo.dao.ContactDto;
import jcg.zheng.demo.querydsldemo.dao.ContactParams;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import jcg.zheng.demo.querydsldemo.dao.ContactQuerydslDao;
import jcg.zheng.demo.querydsldemo.entity.Contact;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class ContactQuerydslDaoImplTest {
	
	@Autowired
	private ContactQuerydslDao contactDao;

	@Test
	public void testPaginatedAndFiltered() {
        ContactParams contactParams = new ContactParams();
        contactParams.setCompanyName("Test");

        Pageable pageable = new PageRequest(5, 5, Sort.Direction.ASC, "companyName");

		Page<ContactDto> found = contactDao.getPaginatedAndFiltered(contactParams, pageable);
		assertTrue(found.getContent().isEmpty());
	}
}
