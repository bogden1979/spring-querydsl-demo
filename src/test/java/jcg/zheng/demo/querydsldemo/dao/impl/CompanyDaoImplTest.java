package jcg.zheng.demo.querydsldemo.dao.impl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import jcg.zheng.demo.querydsldemo.TestData;
import jcg.zheng.demo.querydsldemo.dao.CompanyDao;
import jcg.zheng.demo.querydsldemo.entity.Company;
import jcg.zheng.demo.querydsldemo.entity.CompanyType;
import jcg.zheng.demo.querydsldemo.service.CompanyService;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)

public class CompanyDaoImplTest extends TestData {

	@Autowired
	private CompanyService companyService;

	@Autowired
	private CompanyDao comDao;

	@Test
	public void it_should_find_null() {
		String companyName = "Bad Company";
		Company foundCom = comDao.findByName(companyName);
		assertNull(foundCom);
	}

	@Test
	public void it_should_find_after_save() {
		String companyName = "Zheng Company";
		companyService.save(buildTestCompany(companyName, CompanyType.CUSTOMER));
		Company foundCom = comDao.findByName(companyName);
		assertNotNull(foundCom);
		assertEquals(companyName, foundCom.getName());
		assertEquals(CompanyType.CUSTOMER, foundCom.getType());
	}

}
