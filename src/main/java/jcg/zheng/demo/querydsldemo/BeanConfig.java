package jcg.zheng.demo.querydsldemo;

import com.querydsl.jpa.impl.JPAQueryFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;

@Configuration
public class BeanConfig {
    @Bean
    public JPAQueryFactory sqlQueryFactory(EntityManager entityManager) {
        return new JPAQueryFactory(entityManager);
    }
}
