package jcg.zheng.demo.querydsldemo.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ContactQuerydslDao {
	Page<ContactDto> getPaginatedAndFiltered(ContactParams contactParams, Pageable pageable);
}