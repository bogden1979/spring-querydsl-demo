package jcg.zheng.demo.querydsldemo.dao;

import jcg.zheng.demo.querydsldemo.entity.ContactType;

public class ContactDto {
    private Long id;
    private String firstName;
    private String lastName;
    private ContactType type;
    private String companyName;

    public ContactDto() {
    }

    public ContactDto(Long id, String firstName, String lastName, ContactType type, String companyName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.type = type;
        this.companyName = companyName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ContactType getType() {
        return type;
    }

    public void setType(ContactType type) {
        this.type = type;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
