package jcg.zheng.demo.querydsldemo.dao;

import jcg.zheng.demo.querydsldemo.entity.Company;

public interface CompanyDao {

	Company findByName(String companyName);

}
