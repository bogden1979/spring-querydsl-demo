package jcg.zheng.demo.querydsldemo.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;

import com.querydsl.core.types.Order;
import com.querydsl.core.types.OrderSpecifier;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.JPAExpressions;
import com.querydsl.jpa.impl.JPAQueryFactory;
import jcg.zheng.demo.querydsldemo.dao.ContactDto;
import jcg.zheng.demo.querydsldemo.dao.ContactParams;
import jcg.zheng.demo.querydsldemo.entity.QCompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;

import jcg.zheng.demo.querydsldemo.entity.QContact;
import jcg.zheng.demo.querydsldemo.dao.ContactQuerydslDao;
import jcg.zheng.demo.querydsldemo.entity.Contact;

import static java.util.Objects.nonNull;

/**
 * Compare the JPAQuery to TypedQuery ( non-JpaRepository)
 * 
 * @author Mary.Zheng
 *
 */
@Component
public class ContactQuerydslDaoImpl implements ContactQuerydslDao {

    private JPAQueryFactory jpaQueryFactory;
    private QContact contact = QContact.contact;
    private QCompany company = QCompany.company;

    public ContactQuerydslDaoImpl(@Autowired JPAQueryFactory jpaQueryFactory) {
        this.jpaQueryFactory = jpaQueryFactory;
    }

	@Override
    public Page<ContactDto> getPaginatedAndFiltered(ContactParams contactParams, Pageable pageable) {

		StringPath aliasCompanyName = Expressions.stringPath("company_name");

        JPAQuery<ContactDto> query = jpaQueryFactory.query().select(Projections.constructor(ContactDto.class,
				contact.id,
				contact.firstName,
				contact.lastName,
				contact.type,
				company.name.as(aliasCompanyName)))
                .from(contact, company)
		        .where(contact.companyId.eq(company.id));

        if (nonNull(contactParams.getFirstName())) {
            query.where(contact.firstName.eq(contactParams.getFirstName()));
        }

        if (nonNull(contactParams.getCompanyName())) {
            query.where(company.name.likeIgnoreCase(String.format("%%%s%%", contactParams.getCompanyName())));
        }

        orderBy(pageable, query, aliasCompanyName);
        query.limit(pageable.getPageSize());
        query.offset(pageable.getOffset());

        return new PageImpl<>(query.fetch(), pageable, query.fetchCount());
    }

	@SuppressWarnings("unchecked")
	private void orderBy(Pageable pageable, JPAQuery<ContactDto> query, StringPath aliasCompanyName) {
		List<OrderSpecifier> orderByList = new ArrayList<>();
		for (Sort.Order order : pageable.getSort()) {
			switch (order.getProperty()) {
				case "firstName":
					orderByList.add(new OrderSpecifier(
							Order.valueOf(order.getDirection().toString()),
							QContact.contact.firstName));
					break;
				case "lastName":
					orderByList.add(new OrderSpecifier(
							Order.valueOf(order.getDirection().toString()),
							QContact.contact.lastName));
					break;
				case "contactType":
					orderByList.add(new OrderSpecifier(
							Order.valueOf(order.getDirection().toString()),
							QContact.contact.type));
					break;
                case "companyName":
                    orderByList.add(new OrderSpecifier(
                            Order.valueOf(order.getDirection().toString()),
                            aliasCompanyName));
                    break;
			}
		}

		OrderSpecifier[] array = new OrderSpecifier[orderByList.size()];
		query.orderBy(orderByList.toArray(array));
	}

}
